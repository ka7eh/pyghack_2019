# Champaign Helping Hands

## PYGHACK 2019

### Inspiration

Poverty, homelessness, and household waste are some of the biggest issues in most communities. We wanted to create a tool that can address some of those issues through a user-friendly interface.

### What it does

The web app allows people to submit a request for needed items. The partner organizations will administer those request and can approve or reject them. The requests then will show up anonymously to the community and people can contribute to and fulfill those requests by dropping their donations at the partner organizations drop-off places. Those organization will facilitate the transfer of donated items to the requesters.

## How we built it

We used JS and React framework to set up a mockup draft of the tool. It still requires work to develop and implement the backend.

## What's next for Champaign Helping Hands

Developing and hooking up the back would be the highest priority.
We can envision adding many other features to improve the user experience and make the work of partner organizations easier.

