import { fromJS } from 'immutable'

import { items as itemsFixture } from '../fixtures'
import { ACTIONS } from '../actions/items'

const INITIAL_STATE = fromJS({
    items: itemsFixture
})

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case ACTIONS.SUBMIT_REQUEST:
            return state
        default:
            return state
    }

}
