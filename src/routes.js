import Index from './components/Index'
import About from './components/About'

const routes = {
    "/": { exact: true, component: Index },
    "/about": { component: About}
}

export default routes
