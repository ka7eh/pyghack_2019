export const ACTIONS = {
    UPDATE_LOADING_STATUS: 'UPDATE_LOADING_STATUS',
    UPDATE_VIEW: 'UPDATE_VIEW'
}

export const VIEWS = {
    ADMIN: 'ADMIN',
    INDEX: 'INDEX',
    ITEMS: 'ITEMS',
    LOGIN: 'LOGIN',
    REQUEST: 'REQUEST'
}

const updateLoadingStatus = (isLoading) => ({
    type: ACTIONS.UPDATE_LOADING_STATUS,
    isLoading
})

const updateView = (view) => ({
    type: ACTIONS.UPDATE_VIEW,
    view
})

export default {
    updateLoadingStatus,
    updateView
}
