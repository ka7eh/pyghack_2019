export const ACTIONS = {
    SUBMIT_REQUEST: 'SUBMIT_REQUEST'
}

const submitRequest = (data) => ({
    type: ACTIONS.SUBMIT_REQUEST,
    data
})

export default {
    submitRequest
}
